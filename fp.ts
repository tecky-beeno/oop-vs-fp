export type Teacher = {
  name: string;
  students: Student[];
};

export type Student = {
  name: string;
  totalScore: number;
};

export function takeExam(student: Student, score: number) {
  student.totalScore += score;
}

export function takeStudent(teacher: Teacher, student: Student) {
  teacher.students.push(student);
}

export function getTotalScore(students: Student[]): number {
  return students.reduce((acc, cur) => acc + cur.totalScore, 0);
}

export function report({ name, students }: Teacher) {
  console.log(
    `Teacher ${name} has ${
      students.length
    } students, with total score: ${getTotalScore(students)}`,
  );
}

let alex: Teacher = { name: 'Alex', students: [] };

let alice: Student = { name: 'Alice', totalScore: 0 };
let bob: Student = { name: 'Bob', totalScore: 0 };
// alex.students.push(alice,bob)
takeStudent(alex, alice);
takeStudent(alex, bob);

takeExam(alice,80)
takeExam(bob,70)

report(alex)