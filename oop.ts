export class Student {
  private totalScore: number = 0;
  constructor(public name: string) {}
  takeExam(score: number) {
    this.totalScore += score;
  }
  getTotalScore() {
    return this.totalScore;
  }
}

export class Teacher {
  students: Student[] = [];
  constructor(public name: string) {}
  getTotalScore() {
    let sum = 0;
    for (let i = 0; i < this.students.length; i++) {
      sum += this.students[i].getTotalScore();
    }
    return sum;
  }
  takeStudent(student: Student) {
    this.students.push(student);
  }
  report() {
    console.log(
      'Teacher',
      this.name,
      'has',
      this.students.length,
      'students, with total score:',
      this.getTotalScore(),
    );
  }
}

let alex = new Teacher('Alex');

let alice = new Student('Alice');
alex.takeStudent(alice);
alice.takeExam(80);

let bob = new Student('Bob');
alex.takeStudent(bob);
bob.takeExam(90);

alex.report();
